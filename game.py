import pygame
import random
import pickle


class Game(object):
    block_width = 100
    block_height = 100
    score = 0
    moves = 0
    MAX_MOVES = 500
    actual_moves_made = 0

    def __init__(self, width: int = 4, height: int = 4, headless: bool = True):
        self.width = width
        self.height = height
        self.headless = headless

        if not headless:
            pygame.init()
            self.screen = pygame.display.set_mode((width * self.block_width, width * self.block_height))
            pygame.display.set_caption(f"SnakeySnakeFace - {self.score} points")

        # Generate board
        # Init board all empty
        self.color_grid = [['grey' for _ in range(width)] for _ in range(height)]

        # Set player position to a random pos
        self.player = self.random_pos()
        if not headless:
            self.color_grid[self.player['y']][self.player['x']] = "black"

        # Generate apple pos
        self.apple = self.random_pos()
        while self.apple == self.player:
            #  print("Regenerating apple pos")
            self.apple = self.random_pos()

        if not headless:
            self.color_grid[self.apple['y']][self.apple['x']] = "red"

        # Generate poisenous apple pos
        self.badapple = self.random_pos()
        while self.badapple == self.player or self.badapple == self.apple:
            #  print("Regenerating badapple pos")
            self.badapple = self.random_pos()

        if not headless:
            self.color_grid[self.badapple['y']][self.badapple['x']] = "yellow"

    def play(self):
        #  for event in pygame.event.get():
        #      if event.type == pygame.QUIT:
        #          pygame.quit()
        #          return

        self._draw_grid(self.screen, self.color_grid)
        pygame.display.flip()

    def move(self, dir: str) -> bool:
        # Reset the current player position to grey
        self.color_grid[self.player['y']][self.player['x']] = 'grey'

        # Update player's position based on the direction
        if dir == "left" and self.player['x'] > 0:
            self.player['x'] -= 1
            self.actual_moves_made += 1
        elif dir == "right" and self.player['x'] < self.width - 1:
            self.player['x'] += 1
            self.actual_moves_made += 1
        elif dir == "up" and self.player['y'] > 0:
            self.player['y'] -= 1
            self.actual_moves_made += 1
        elif dir == "down" and self.player['y'] < self.height - 1:
            self.player['y'] += 1
            self.actual_moves_made += 1
        else:
            # So no move was made, which also means no move from here is going to be made since the input is the exact same.
            self.quit()
            return False

        # Do the logic of the player hitting the apple
        if self.player == self.apple:
            self.score += 1

            # Generate apple pos
            self.apple = self.random_pos()
            while self.apple == self.player or self.apple == self.badapple:
                self.apple = self.random_pos()

        if self.player == self.badapple:
            #  print(f"You died! Your score was: {self.score}.\n")
            self.quit()
            return False

        # Set the new player position to black
        self.color_grid[self.player['y']][self.player['x']] = "black"
        self.color_grid[self.apple['y']][self.apple['x']] = "red"

        # Do moves logic
        self.moves += 1

        # Redraw the grid and introduce a delay
        if not self.headless:
            pygame.display.set_caption(f"SnakeySnakeFace - {self.score} points")
            self._draw_grid(self.screen, self.color_grid)

            self.play()

        if self.moves >= self.MAX_MOVES:
            #  print(f"You ran out of moves! Your score was: {self.score}.\n")
            return False

        return True

    def quit(self):
        return
        pygame.quit()

    def _draw_grid(self, screen, color_grid):
        for i in range(self.height):
            for j in range(self.width):
                pygame.draw.rect(screen, color_grid[i][j], (j * self.block_width, i * self.block_height, self.block_width, self.block_height))

    def random_pos(self):
        x = random.randint(0, self.width - 1)
        y = random.randint(0, self.height - 1)

        return {'x': x, 'y': y}

    def save_to_file(self, filename):
        with open(filename, 'wb') as file:
            pickle.dump(self, file)

    def make_head(self):
        self.headless = False
        pygame.init()
        self.screen = pygame.display.set_mode((self.width * self.block_width, self.width * self.block_height))
        pygame.display.set_caption(f"SnakeySnakeFace - {self.score} points")
        self.color_grid[self.apple['y']][self.apple['x']] = "red"
        self.color_grid[self.player['y']][self.player['x']] = "black"
        self.color_grid[self.badapple['y']][self.badapple['x']] = "yellow"


if __name__ == "__main__":
    game = Game()
    game.play()

    running = True
    while running:
        for event in pygame.event.get():
            if event.type == pygame.QUIT:
                running = False
            elif event.type == pygame.KEYDOWN:
                if event.key == pygame.K_LEFT:
                    running = game.move("left")
                elif event.key == pygame.K_RIGHT:
                    running = game.move("right")
                elif event.key == pygame.K_UP:
                    running = game.move("up")
                elif event.key == pygame.K_DOWN:
                    running = game.move("down")

    game.quit()

    quit()
