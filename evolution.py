from bot import Bot
from game import Game
from neuralnetwork import NeuralNetwork
import pygame
import copy
import random
import pickle
import numpy as np
from typing import Optional, Tuple


class Evolution(object):
    def __init__(self, population_size: int, neural_network: Optional[NeuralNetwork] = None):
        self.games = [Game() for _ in range(population_size)]

        if neural_network:
            self.neural_networks = [neural_network.copy() for _ in range(population_size)]
        else:
            self.neural_networks = [NeuralNetwork() for _ in range(population_size)]

        self.population = [Bot(self.games[i], self.neural_networks[i]) for i in range(population_size)]

    def run_generation(self):
        processed_bots = []
        for bot in self.population:
            processed_bots.append(Evolution.EvaluateBot(bot))

        self.population = processed_bots

    def run(self, generations: int = 5, percentage_survivors: int = 50):
        self.original_game = Game()
        for bot in self.population:
            bot.game = copy.deepcopy(self.original_game)

        for generation_number in range(generations):
            self.run_generation()

            #  std_weights, std_biases = self.check_diversity()

            #  calc_scores = lambda bot: bot.game.score + (3 if 0 < bot.game.player['y'] < 3 and 0 < bot.game.player['x'] < 3 else 0)
            #  calc_scores = lambda bot: bot.game.actual_moves_made
            calc_scores = lambda bot: bot.game.score + bot.game.actual_moves_made / 500
            # Currently artificially boost if the bot ended in the middle of the board to discourage just going left all the time etc.
            scores = [calc_scores(bot) for bot in self.population]

            #  print(f"Generation {generation_number:4}/{generations} ({generation_number/generations*100:3.0f})%: {(sum(scores) / len(scores)):10.5f}. Top 10 scores: {str(sorted(scores, reverse=True))[:20]}... Std Weights: {std_weights}, Std biases: {std_biases}. Avg pairwise difference: {self.average_pairwise_difference()}")
            print(f"Generation {generation_number:4}/{generations} ({generation_number/generations*100:3.0f})%: {(sum(scores) / len(scores)):10.5f}. Top 10 scores: {str(sorted(scores, reverse=True))[:20]}...")

            #  self.population.sort(reverse=True, key=lambda bot: bot.game.score)
            self.population.sort(reverse=True, key=calc_scores)

            survivors = self.population[:len(self.population)*percentage_survivors//100]
            best_score = calc_scores(survivors[0])

            # Copy and slightly alter the copies
            for bot in survivors:
                bot.game = copy.deepcopy(self.original_game)  # Now they start with the same setup
                bot.alive = True

            new_generation = []
            for copy_step in range(1, 100//percentage_survivors):
                for survivor in survivors:
                    new_generation.append(survivor.copy())

            # Mutate half of them normally, a quarter stronger, and the last quarter the strongest
            random.shuffle(new_generation)
            for bot_id, bot in enumerate(new_generation):
                #  bot.neural_network.mutate(0.2)
                if bot_id < len(new_generation)/2:
                    bot.neural_network.mutate(0.1)
                elif bot_id < len(new_generation)/4*3:
                    bot.neural_network.mutate(0.2)
                else:
                    bot.neural_network.mutate(0.4)

            self.population = survivors + new_generation

        # Finally return the last best survivor
        best_survivor = survivors[0]

        return best_survivor, best_score

    def check_diversity(self) -> Tuple[float, float]:
        all_weights = []
        all_biases = []

        for nn in [bot.neural_network for bot in self.population]:
            weights, biases = nn.get_all_weights_and_biases()
            all_weights.extend(weights)
            all_biases.extend(biases)

        std_weights = np.std(all_weights)
        std_biases = np.std(all_biases)

        return std_weights, std_biases

    def average_pairwise_difference(self) -> float:
        neural_networks = [bot.neural_network for bot in self.population]
        total_differences = 0.0
        comparisons = 0.0

        for i in range(len(neural_networks)):
            for j in range(i+1, len(neural_networks)):
                # Flatten and concatenate weights for neural network i
                weights_i_input_to_hidden = np.array(neural_networks[i].weights_input_to_hidden).flatten()
                weights_i_hidden_to_output = np.array(neural_networks[i].weights_hidden_to_output).flatten()
                weights_i = np.concatenate([weights_i_input_to_hidden, weights_i_hidden_to_output])

                # Flatten and concatenate weights for neural network j
                weights_j_input_to_hidden = np.array(neural_networks[j].weights_input_to_hidden).flatten()
                weights_j_hidden_to_output = np.array(neural_networks[j].weights_hidden_to_output).flatten()
                weights_j = np.concatenate([weights_j_input_to_hidden, weights_j_hidden_to_output])

                # Calculate differences for weights
                weight_difference = np.linalg.norm(weights_i - weights_j)

                # Concatenate biases for neural network i and j
                biases_i = np.concatenate([neural_networks[i].biases_hidden, neural_networks[i].biases_output])
                biases_j = np.concatenate([neural_networks[j].biases_hidden, neural_networks[j].biases_output])

                # Calculate differences for biases
                bias_difference = np.linalg.norm(biases_i - biases_j)

                total_differences += float(weight_difference + bias_difference)
                comparisons += 1

        return total_differences / comparisons

    @classmethod
    def EvaluateBot(cls, bot: Bot) -> Bot:
        while bot.alive:
            bot.make_move()

        return bot


def load_neural_network(filename: str) -> NeuralNetwork:
    with open(filename, 'rb') as file:
        network = pickle.load(file)
    return network


if __name__ == "__main__":
    nn = load_neural_network("bestnn.txt")
    evolution = Evolution(40)

    best_bot, best_score = evolution.run(50)
    best_bot.neural_network.save_to_file("bestnn.txt")
    pygame.quit()
