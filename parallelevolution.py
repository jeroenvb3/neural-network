from multiprocessing import Pool, cpu_count
from evolution import Evolution
import pickle
from typing import Optional
from neuralnetwork import NeuralNetwork


class ParallelEvolution(Evolution):
    def __init__(self, population_size: int, neural_network: Optional[NeuralNetwork] = None):
        super().__init__(population_size, neural_network)

    def run_generation(self):
        # Use multiprocessing to evaluate bots in parallel
        with Pool(processes=cpu_count()-1) as pool:
            self.population = pool.map(Evolution.EvaluateBot, self.population)

    # The rest of the methods remain the same as in the Evolution class


def load_neural_network(filename: str) -> NeuralNetwork:
    with open(filename, 'rb') as file:
        network = pickle.load(file)
    return network


if __name__ == "__main__":
    nn = load_neural_network("bestnn.txt")
    pe = ParallelEvolution(60, nn)
    #  pe = ParallelEvolution(1000, nn)
    print("Initialized parallel evolution.")
    best_bot, best_score = pe.run(20)
    best_bot.neural_network.save_to_file("bestnn.txt")
    pe.original_game.save_to_file("bestgame.txt")
    print(f"Best bot score: {best_score}")
