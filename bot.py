from game import Game
from neuralnetwork import NeuralNetwork
import time
import copy


def get_input_values_from_game(game: Game):
    lookup = {
        "grey": [1, 0, 0, 0],
        "black": [0, 1, 0, 0],
        "red": [0, 0, 1, 0],
        "yellow": [0, 0, 0, 1]
            }
    translated = [lookup[item] for sublist in game.color_grid for item in sublist]
    flattened = [item for sublist in translated for item in sublist]

    return flattened


def move_from_output(output_index: int):
    return ["left", "right", "up", "down"][output_index]


class Bot(object):
    def __init__(self, game: Game, neural_network: NeuralNetwork):
        self.game = game
        self.neural_network = neural_network
        self.alive = True

    def make_move(self):
        #  print(f"Making a move for bot {self} on board {self.game}")
        input_array = get_input_values_from_game(self.game)
        #  print(f"Input array: {input_array}")
        output_index = self.neural_network.get_decision(input_array)
        #  print(f"Output array: {output_array}")
        move = move_from_output(output_index)
        #  print(f"Move: {move}")

        self.alive = self.game.move(move)

    def copy(self):
        new_bot = Bot(copy.deepcopy(self.game), self.neural_network.copy())

        return new_bot


if __name__ == "__main__":
    game = Game()
    nn = NeuralNetwork()

    bot = Bot(game, nn)
    game.play()

    for i in range(5):
        bot.make_move()
        time.sleep(1)

        if not bot.alive:
            quit()
