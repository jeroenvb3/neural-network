import numpy as np
import pickle


def sigmoid(z: float):
    return 1 / (1 + np.exp(-z))


class NeuralNetwork(object):
    def __init__(self, input_nodes: int = 64, hidden_layer_one_nodes: int = 128, output_nodes: int = 4):
        self.input_nodes = input_nodes
        self.hidden_layer_one_nodes = hidden_layer_one_nodes
        self.output_nodes = output_nodes

        # Initialize weights and biases for input to hidden layer
        self.weights_input_to_hidden = np.random.uniform(-1, 1, (input_nodes, hidden_layer_one_nodes))
        self.biases_hidden = np.zeros(hidden_layer_one_nodes)

        # Initialize weights and biases for hidden to output layer
        self.weights_hidden_to_output = np.random.uniform(-1, 1, (hidden_layer_one_nodes, output_nodes))
        self.biases_output = np.zeros(output_nodes)

    def get_decision(self, input_array: np.ndarray) -> np.int64:
        # Calculate hidden layer
        hidden_layer = sigmoid(np.dot(input_array, self.weights_input_to_hidden) + self.biases_hidden)

        # Calculate output layer
        output_layer = sigmoid(np.dot(hidden_layer, self.weights_hidden_to_output) + self.biases_output)
        #  print(output_layer)

        return np.argmax(output_layer)

    def mutate(self, strength: float = 0.05):
        self.weights_input_to_hidden += np.random.normal(0, strength, self.weights_input_to_hidden.shape)
        self.weights_hidden_to_output += np.random.normal(0, strength, self.weights_hidden_to_output.shape)
        self.biases_hidden += np.random.normal(0, strength, self.biases_hidden.shape)
        self.biases_output += np.random.normal(0, strength, self.biases_output.shape)

        # Constrain the weights and biases between -1 and 1
        self.weights_input_to_hidden = np.clip(self.weights_input_to_hidden, -1, 1)
        self.weights_hidden_to_output = np.clip(self.weights_hidden_to_output, -1, 1)
        self.biases_hidden = np.clip(self.biases_hidden, -1, 1)
        self.biases_output = np.clip(self.biases_output, -1, 1)

    def save_to_file(self, filename: str):
        with open(filename, 'wb') as file:
            pickle.dump(self, file)

    def copy(self):
        new_nn = NeuralNetwork()
        new_nn.weights_input_to_hidden = np.copy(self.weights_input_to_hidden)
        new_nn.weights_hidden_to_output = np.copy(self.weights_hidden_to_output)
        new_nn.biases_hidden = np.copy(self.biases_hidden)
        new_nn.biases_output = np.copy(self.biases_output)
        return new_nn

    def get_all_weights_and_biases(self):
        all_weights = []
        all_biases = []

        for layer_weights in self.weights_input_to_hidden:
            all_weights.extend(layer_weights)
        for layer_weights in self.weights_hidden_to_output:
            all_weights.extend(layer_weights)

        all_biases.extend(self.biases_hidden)
        all_biases.extend(self.biases_output)

        return all_weights, all_biases


if __name__ == "__main__":
    nn = NeuralNetwork()
    print(nn.weights_input_to_hidden)
    print()
